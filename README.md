### 简介
`vs-proxy`是[vsearcher库](https://pypi.org/project/vsearcher/)的一个应用案例系统的`前端代理服务程序`。该项目的研发背景可[点击查看](http://breath57.gitee.io/vs-docs/demo/%E4%BA%91%E9%83%A8%E7%BD%B2%E9%97%AE%E9%A2%98.html#%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%88)。

### 使用方式
[点击查看](http://breath57.gitee.io/vs-docs/demo/%E4%BA%91%E9%83%A8%E7%BD%B2%E9%97%AE%E9%A2%98.html#vs-proxy%E7%9A%84%E4%BD%BF%E7%94%A8)

### API文档(HTTP)
[点击查看](http://breath57.gitee.io/vs-docs/demo/vs-proxy-api/)