# 永远不上传到服务器 | 存机密信息
DEBUG = False
HOST = '0.0.0.0'
PORT = 5001
REMOTE_BASE_URL = 'http://127.0.0.1:5000'  # 上传内容到远端的服务器
# SQLALCHEMY_DATABASE_URI = 'mysql://root:root@localhost:3306/flask_test'
# SQLALCHEMY_DATABASE_URI = 'sqlite:///app/models/db/vsearcher.db'
SQLALCHEMY_DATABASE_URI = 'sqlite:///models/db/vsearcher.db'
JSON_AS_ASCII = True  # @NOTING flask默认是True,

ACCESS_CONTROL_ALLOW_ORIGIN = '*'  # @RISK 全局跨域
