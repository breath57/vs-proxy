from flask import Blueprint
from app.libs.forms import TypeForm, UploadByPathForm, UploadFileForm, VSConfigForm
from app.libs.enums import RType
from app.service.course import CourseService
from app.service.chapter import ChapterService
from app.service.video import VideoService

bp_resource = Blueprint('resource', __name__, url_prefix='/resource')
'''
TODO：(Completed)
    负责将文件路径转换为，真实的二进制视频对象，并且组织起来，传递给真实的服务器进行处理

    参数校验：其实可以不用，因为最终处理的服务器处于云端，它拥有校验机制


主要功能：
    1. root_dir -> 有组织的对象 | 
        实现方案:
            1. 云端提供, a. 添加一个视频文件到某个目录下的方法, 
                
                b. 提供创建章节，和 c. 创建课程的方法：(优点：控制的粒度程度更低，更佳的灵活)
                a. 假设：(该方法的实现细节，需要设计，有点复杂)
                    url： http://domain_url/v1/upload/video
                    params： 
                        type: 1 | 2 | 3  当 type为3, chapter_x和course_name都不能为空
                        chapter_id: 第一次
                        chapter_name
                        course_id
                        course_name
                        file: 视频文件对象
                b.和c. 
                    url: http://domain_url/v1/create
                    params:
                        type:
                        name: 资源的名称
                
            2. 本地读取目录下的视频文件组织结构, 然后进行for循环, 批量处理
            3. 并发请求, 进行处理, (过程异步 +  结果同步), 返回结果
'''


@bp_resource.route('/upload', methods=['POST'])
def upload_by_path():
    type_form = TypeForm(get_token=True).check_validate()
    vsConfigForm = VSConfigForm(False).check_validate()
    r_type = RType(type_form.type.data)
    if r_type is RType.CHAPTER:
        up_form = UploadByPathForm(get_token=False).check_validate()
        up_form.vsConfigForm = vsConfigForm
        return ChapterService.upload_by_path(up_form).response()
    elif r_type is RType.COURSE:
        up_form = UploadByPathForm(get_token=False).check_validate()
        up_form.vsConfigForm = vsConfigForm
        return CourseService.upload_by_path(up_form).response()
    elif r_type is RType.VIDEO:
        up_form = UploadFileForm(False).check_validate()
        up_form.vsConfigForm = vsConfigForm
        return VideoService.upload_by_file(up_form).response()
