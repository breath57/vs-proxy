

from flask import Blueprint
from app.libs.exception import Success
from app.libs.forms import VSConfigForm
from app.models.config import Config
from app.vo import ConfigVO

bp_config = Blueprint('config', __name__, url_prefix='/config')


@bp_config.route('/get', methods=['POST', ' GET'])
def get():
    config = Config.filter(Config.id == 1).first()
    config = ConfigVO(config)
    if config.step == 0:
        config.step = 'fps'
    return config.response()


@bp_config.route('/set', methods=['POST'])
def set():
    configForm = VSConfigForm(False)
    with Config.auto_commit() as context:
        config = Config.filter(Config.id == 1).first()
        config = config.set_attr(configForm)
        if configForm.step.data == 'fps':
            config.step = 0
        context.session.add(config)  # 更新
    return Success(msg=dict(
        msg='参数更新成功',
        config=configForm.data
    )).response()
