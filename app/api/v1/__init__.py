from flask import Blueprint
# 导入文件
bp_v1 = Blueprint('v1', __name__, url_prefix='/v1')


def cite_routes():  # 注册某个模块下的蓝图
    from .resource import bp_resource
    from .config import bp_config
    bp_v1.register_blueprint(bp_resource)
    bp_v1.register_blueprint(bp_config)


cite_routes()
