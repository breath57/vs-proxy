

from enum import Enum


class RType(Enum):

    VIDEO = 1
    CHAPTER = 2
    COURSE = 3

    @staticmethod
    def get_kv_dict():
        return {e.name: e.value for e in RType}
