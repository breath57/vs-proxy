from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import Session


db = SQLAlchemy()


class AutoCommit:
    """ 自动提交 """

    def __init__(self, db: SQLAlchemy):
        self._db = db
        self._session = db.session

    @property
    def session(self) -> Session:
        return self._session

    @property
    def db(self) -> SQLAlchemy:
        return self._db

    def __enter__(self):
        # if self.reset:
        #     self.db.drop_all()
        #     self.db.create_all()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._db.session.commit()
