from tokenize import String
from sqlalchemy import Column, Float, Integer
from .base import BaseModel
# from werkzeug.security import check_password_hash, generate_password_hash
# @RISK 后面再考虑加密


class Config(BaseModel):

    id = Column(Integer, primary_key=True, autoincrement=True)

    step = Column(Integer, default=0, nullable=True,
                  comment="遍历的帧间隔， 0: 代表取值为视频的FPS（帧率）")

    speed = Column(Float, default=1, nullable=True,
                   comment="使得真正的step为step*speed_x")
