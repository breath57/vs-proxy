from flask import request
from app.libs.exception import APIException
from app.libs.forms import UploadFileForm
from app.libs.utils import HttpHelper
from app.ro import UploadPureVideoParam
from .base import BaseService

class VideoService(BaseService):

    @classmethod
    def upload_by_file(cls, form: UploadFileForm) -> APIException:
        url = '/v1/resource/upload'
        param = UploadPureVideoParam().set_attrs(dict(
            type=form.type.data,
            step=form.vsConfigForm.step.data,
            speed=form.vsConfigForm.speed.data,
            chapter_id=form.chapter_id.data,
            course_id=form.course_id.data
        ))
        file = request.files.get('file')
        resp = HttpHelper.post(
            url=url, params=param.__dict__, files={'file': (file.filename, file)})
        return HttpHelper.resp2APIException(resp=resp)
