import json
""" 发起Http请求的参数对象 """


class BaseParam():

    def set_attrs(self, dict_or_o):
        if not isinstance(dict_or_o, dict):
            dict_or_o = dict(dict_or_o)
        for k, v in dict_or_o.items():  # 对象.__dict__ 并不会包含非属性的内容
            setattr(self, k, v)
        return self

    def jsonStr(self):
        json.dumps(self.__dict__)


class UploadPureVideoParam(BaseParam):
    file: str
    type: int
    step: int
    speed: float
    chapter_id: int
    course_id: int


class CreateRemoteDirParam(BaseParam):
    name: str
    type: int
    course_id: int
